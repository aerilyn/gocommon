package config

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"strconv"
	"strings"

	yaml "gopkg.in/yaml.v3"
)

const (
	defaultPathConfig = "config/config.yaml"
)

func SetConfig() Config {
	config := SetDataConfigFromYAML()
	config = SetValueFromEnv(config, nil, config, "")
	return config
}

func SetDataConfigFromYAML() Config {
	var c Config
	fh, err := os.Open(defaultPathConfig)
	if err != nil {
		panic(fmt.Sprintf("weeee: %v", err))
	}
	defer fh.Close()
	b, err := ioutil.ReadAll(fh)
	if err != nil {
		panic(fmt.Sprintf("weeee 2: %v", err))
	}

	if err := yaml.Unmarshal(b, &c); err != nil {
		log.Fatal(err)
	}
	return c
}

func SetValueFromEnv(iface interface{}, sf *reflect.Value, data Config, prefix string) Config {
	defaultPrefix := prefix
	ifv := reflect.ValueOf(iface)
	ift := reflect.TypeOf(iface)

	for i := 0; i < ift.NumField(); i++ {
		var editObj reflect.Value
		prefix = defaultPrefix
		envVar := ""
		t := ift.Field(i)
		v := ifv.Field(i)
		d := sf
		if d == nil {
			editObj = reflect.ValueOf(&data).Elem().Field(i)
		} else {
			editObj = d.FieldByName(t.Name)
		}

		switch v.Kind() {
		case reflect.Struct:
			if prefix == "" {
				prefix = fmt.Sprintf("%v", t.Name)
			} else {
				prefix = fmt.Sprintf("%v_%v", prefix, t.Name)
			}

			SetValueFromEnv(v.Interface(), &editObj, data, prefix)
		case reflect.Slice:
			continue
		default:
			envVar = strings.ToUpper(fmt.Sprintf("%v_%v", prefix, t.Name))
			valueEnv := os.Getenv(envVar)
			if valueEnv == "" {
				continue
			}
			switch v.Kind() {
			case reflect.String:
				editObj.SetString(valueEnv)
			case reflect.Int:
				val, err := strconv.Atoi(valueEnv)
				if err != nil {
					panic(fmt.Sprintf("error when set value int %v", envVar))
				}
				editObj.SetInt(int64(val))
			case reflect.Float64:
				val, err := strconv.ParseFloat(strings.TrimSpace(valueEnv), 64)
				if err != nil {
					panic(fmt.Sprintf("error when set value float %v", envVar))
				}
				editObj.SetFloat(val)
			case reflect.Bool:
				val, err := strconv.ParseBool(valueEnv)
				if err != nil {
					panic(fmt.Sprintf("error when set value bool %v", envVar))
				}
				editObj.SetBool(val)
			}
		}
	}
	return data
}
