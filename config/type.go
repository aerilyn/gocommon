package config

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"strconv"
	"strings"

	yaml "gopkg.in/yaml.v3"
)

const (
	ObjConfigName string = "Config"
	IsPrinted     bool   = true
)

type Config struct {
	Service    Service
	Datasource Datasource
}

type Datasource struct {
	MongoDB []MongoDB
	Mysql   []Mysql
	Redis   []Redis
}

type MongoDB struct {
	Addr     string
	Database string
	Name     string
}

type Mysql struct {
	Addr     string
	Database string
	Name     string
}

type Redis struct {
	Addr     string
	Database string
	Name     string
}

type Service struct {
	Name string
	Env  string
}

func (c *Config) SetDataConfigFromYAML() {
	fh, err := os.Open(defaultPathConfig)
	if err != nil {
		panic(fmt.Sprintf("weeee: %v", err))
	}
	defer fh.Close()
	b, err := ioutil.ReadAll(fh)
	if err != nil {
		panic(fmt.Sprintf("weeee 2: %v", err))
	}

	if err := yaml.Unmarshal(b, c); err != nil {
		log.Fatal(err)
	}
}

func (c *Config) SetValueFromEnv(iface interface{}, sf *reflect.Value, prefix, x string) {
	defaultPrefix := prefix
	ifv := reflect.ValueOf(iface)
	ift := reflect.TypeOf(iface)

	for i := 0; i < ift.NumField(); i++ {
		var editObj reflect.Value
		prefix = defaultPrefix
		envVar := ""
		t := ift.Field(i)
		v := ifv.Field(i)
		c.printLog("===========================================================================")
		c.printLog("x => %v \t ift.NumField => %v", x, ift.NumField())
		c.printLog("sf => %v, d == nil (%v)", sf, sf == nil)
		if sf != nil {
			c.printLog("sf type => %v", sf.Type().Name())
		}
		c.printLog("i => %v", i)
		c.printLog("type => %v \t kind  => %v \t name  => %v", v.Type().Name(), v.Kind(), t.Name)
		if sf == nil {
			if prefix == "" && v.Kind() == reflect.Struct && i == 0 {
				c.printLog("masuk sini editObj config")
				editObj = reflect.ValueOf(c).Elem()
			} else {
				editObj = reflect.ValueOf(c).Elem().Field(i)
			}
			c.printLog("masuk sini coi | d => %v interface  => %v", t.Name, editObj.Interface())
		} else {
			c.printLog("d => %v interface  => %v", t.Name, sf.Interface())
			editObj = sf.FieldByName(t.Name)
			if !editObj.IsValid() {
				c.printLog("masuk IsInvalid")
				editObj = *sf
			}
			c.printLog("editObj => %v", editObj.Interface())
		}

		switch v.Kind() {
		case reflect.Struct:
			if prefix == "" || (prefix == "Config" && v.Kind() == reflect.Struct && i == 0) {
				prefix = fmt.Sprintf("%v", t.Name)
			} else {
				prefix = fmt.Sprintf("%v_%v", prefix, t.Name)
			}

			c.SetValueFromEnv(v.Interface(), &editObj, prefix, "1")
		case reflect.Slice:
			continue
		default:
			envVar = strings.ToUpper(fmt.Sprintf("%v_%v", prefix, t.Name))
			c.printLog("envVar %v", envVar)
			valueEnv := os.Getenv(envVar)
			if valueEnv == "" {
				continue
			}
			switch v.Kind() {
			case reflect.String:
				editObj.SetString(valueEnv)
			case reflect.Int:
				val, err := strconv.Atoi(valueEnv)
				if err != nil {
					panic(fmt.Sprintf("error when set value int %v", envVar))
				}
				editObj.SetInt(int64(val))
			case reflect.Float64:
				val, err := strconv.ParseFloat(strings.TrimSpace(valueEnv), 64)
				if err != nil {
					panic(fmt.Sprintf("error when set value float %v", envVar))
				}
				editObj.SetFloat(val)
			case reflect.Bool:
				val, err := strconv.ParseBool(valueEnv)
				if err != nil {
					panic(fmt.Sprintf("error when set value bool %v", envVar))
				}
				editObj.SetBool(val)
			}
		}
	}
}

func (c *Config) printLog(msg string, params ...interface{}) {
	if IsPrinted {
		fmt.Println(fmt.Sprintf(msg, params...))
	}
}
