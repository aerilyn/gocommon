module gitlab.com/aerilyn/gocommon

go 1.16

require (
	github.com/golang/mock v1.6.0
	github.com/stretchr/testify v1.7.0 // indirect
	go.mongodb.org/mongo-driver v1.9.0
	gopkg.in/yaml.v3 v3.0.1
)
