package mongodb

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type DatabaseConnection map[string]*MongoDbConnection

var (
	dbConn    = make(DatabaseConnection)
	onceMongo = make(map[string]bool)
)

type MongoDbDatabase interface {
	Collection(name string, opts ...*options.CollectionOptions) MongoDbCollection
}

type MongoDbCollection interface {
	FindOne(ctx context.Context, filter interface{}, opts ...*options.FindOneOptions) MongoDbSingleResult
	InsertOne(ctx context.Context, document interface{}, opts ...*options.InsertOneOptions) (*mongo.InsertOneResult, error)
	UpdateOne(ctx context.Context, filter interface{}, update interface{}, opts ...*options.UpdateOptions) (*mongo.UpdateResult, error)
	Aggregate(ctx context.Context, pipeline interface{}, opts ...*options.AggregateOptions) (MongoDbCursor, error)
	CountDocuments(ctx context.Context, filter interface{}, opts ...*options.CountOptions) (int64, error)
	FindOneAndUpdate(ctx context.Context, filter interface{}, update interface{}, opts ...*options.FindOneAndUpdateOptions) MongoDbSingleResult
	FindOneAndDelete(ctx context.Context, filter interface{}, opts ...*options.FindOneAndDeleteOptions) MongoDbSingleResult
	InsertMany(ctx context.Context, documents []interface{}, opts ...*options.InsertManyOptions) (*mongo.InsertManyResult, error)
	DeleteOne(ctx context.Context, filter interface{}, opts ...*options.DeleteOptions) (*mongo.DeleteResult, error)
	Find(ctx context.Context, filter interface{}, opts ...*options.FindOptions) (MongoDbCursor, error)
}

type MongoDbSingleResult interface {
	Decode(v interface{}) error
}

type MongoDbCursor interface {
	All(ctx context.Context, results interface{}) error
	Next(ctx context.Context) bool
	Decode(val interface{}) error
}

type mongoSingleResult struct {
	sr *mongo.SingleResult
}

type mongoAggregate struct {
	sr *mongo.Cursor
}

type mongoCollection struct {
	sr *mongo.Collection
}

type MongoDbConnection struct {
	db *mongo.Database
}

func NewSingleton(name, addr, database string) DatabaseConnection {
	if !onceMongo[name] {
		client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(addr))
		if err != nil {
			panic(err.Error())
		}

		dbConn[name] = &MongoDbConnection{client.Database(database)}
	}
	return dbConn
}

func (x *MongoDbConnection) Collection(name string, opts ...*options.CollectionOptions) MongoDbCollection {
	collection := x.db.Collection(name, opts...)
	return &mongoCollection{collection}
}

func (x *mongoCollection) FindOne(ctx context.Context, filter interface{}, opts ...*options.FindOneOptions) MongoDbSingleResult {
	findOne := x.sr.FindOne(ctx, filter, opts...)
	return &mongoSingleResult{findOne}
}

func (x *mongoCollection) UpdateOne(ctx context.Context, filter interface{}, update interface{}, opts ...*options.UpdateOptions) (*mongo.UpdateResult, error) {
	return x.sr.UpdateOne(ctx, filter, update, opts...)
}

func (x *mongoCollection) InsertOne(ctx context.Context, document interface{}, opts ...*options.InsertOneOptions) (*mongo.InsertOneResult, error) {
	return x.sr.InsertOne(ctx, document, opts...)
}

func (x *mongoCollection) CountDocuments(ctx context.Context, filter interface{}, opts ...*options.CountOptions) (int64, error) {
	return x.sr.CountDocuments(ctx, filter, opts...)
}

func (x *mongoCollection) Aggregate(ctx context.Context, pipeline interface{}, opts ...*options.AggregateOptions) (MongoDbCursor, error) {
	aggregate, err := x.sr.Aggregate(ctx, pipeline, opts...)
	return &mongoAggregate{aggregate}, err
}

func (x *mongoCollection) FindOneAndDelete(ctx context.Context, filter interface{}, opts ...*options.FindOneAndDeleteOptions) MongoDbSingleResult {
	findOne := x.sr.FindOneAndDelete(ctx, filter, opts...)
	return &mongoSingleResult{findOne}
}

func (x *mongoCollection) FindOneAndUpdate(ctx context.Context, filter interface{}, update interface{}, opts ...*options.FindOneAndUpdateOptions) MongoDbSingleResult {
	findOne := x.sr.FindOneAndUpdate(ctx, filter, update, opts...)
	return &mongoSingleResult{findOne}
}

func (x *mongoCollection) InsertMany(ctx context.Context, documents []interface{}, opts ...*options.InsertManyOptions) (*mongo.InsertManyResult, error) {
	return x.sr.InsertMany(ctx, documents, opts...)
}

func (x *mongoCollection) DeleteOne(ctx context.Context, filter interface{}, opts ...*options.DeleteOptions) (*mongo.DeleteResult, error) {
	return x.sr.DeleteOne(ctx, filter, opts...)
}

func (x *mongoCollection) Find(ctx context.Context, filter interface{}, opts ...*options.FindOptions) (MongoDbCursor, error) {
	find, err := x.sr.Find(ctx, filter, opts...)
	return &mongoAggregate{find}, err
}

func (x *mongoSingleResult) Decode(v interface{}) error {
	return x.sr.Decode(v)
}

func (x *mongoAggregate) All(ctx context.Context, results interface{}) error {
	return x.sr.All(ctx, results)
}

func (x *mongoAggregate) Next(ctx context.Context) bool {
	return x.sr.Next(ctx)
}

func (x *mongoAggregate) Decode(val interface{}) error {
	return x.sr.Decode(val)
}
