package logger

/*
import (
	"bytes"
	"context"
	"net/http"
	"net/http/httputil"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
)

// UTCFormatter ...
type UTCFormatter struct {
	logrus.Formatter
}

func NewLogging(r *http.Request, dateTimeStart time.Time) StandarLogger {
	var data StandarLogger
	data.RequestID = uuid.New().String()
	data.Service = os.Getenv("SERVICE_NAME")
	data.Endpoint = r.Host
	data.Time = dateTimeStart
	data.RequestMethod = r.Method

	dataRequest := RequestLogger{
		StandarLogger: data,
		Header:        DumpRequest(r),
	}
	logrus.WithField("data", dataRequest).Info(InfoRequest)
	return data
}

// DumpRequest is for get all data request header
func DumpRequest(req *http.Request) string {
	header, err := httputil.DumpRequest(req, true)
	if err != nil {
		return "cannot dump request"
	}

	trim := bytes.ReplaceAll(header, []byte("\r\n"), []byte("   "))
	return string(trim)
}

func Response(ctx context.Context) {
	logrus.SetFormatter(UTCFormatter{&logrus.JSONFormatter{}})
	if d.StatusCode >= 200 && d.StatusCode < 400 {
		logrus.WithField("data", d).Info("apps")
	} else if d.StatusCode >= 400 && d.StatusCode < 500 {
		logrus.WithField("data", d).Warn("apps")
	} else {
		logrus.WithField("data", d).Error("apps")
	}
}

type StandarLogger struct {
	RequestID     string    `json:"requestID"`
	UserID        string    `json:"userID"`
	Time          time.Time `json:"time"`
	File          string    `json:"file"`
	PkgPath       string    `json:"packagePath"`
	Function      string    `json:"function"`
	Info          string    `json:"info"`
	RequestMethod string    `json:"requestMethod"`
	Service       string    `json:"service"`
	Host          string    `json:"host"`
	Endpoint      string    `json:"endpoint"`
}

type RequestLogger struct {
	StandarLogger
	Header string `json:"header"`
}

type ResponseLogger struct {
	StandarLogger
	Response   interface{} `json:"reponse"`
	StatusCode int         `json:"statusCode"`
	ExecTime   float64     `json:"executionTime"`
}

type defaultFields struct {
	filename interface{}
	fn       interface{}
	pkg      interface{}
}

func getDefaultFieldValue() defaultFields {
	pc, file, line, ok := runtime.Caller(2)
	if !ok {
		logrus.Error("Could not get context info for logger!")
		return defaultFields{}
	}
	filename := file[strings.LastIndex(file, "/")+1:] + ":" + strconv.Itoa(line)
	callerFunc := runtime.FuncForPC(pc)
	funcForPC := callerFunc.Name()
	fn := funcForPC[strings.LastIndex(funcForPC, ".")+1:]
	pkg := funcForPC[:strings.LastIndex(funcForPC, ".")-1]
	return defaultFields{
		filename: filename,
		fn:       fn,
		pkg:      pkg,
	}
}
*/
